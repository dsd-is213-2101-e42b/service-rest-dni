from dotenv import dotenv_values
from zeep import Client, helpers

config = dotenv_values('../.env')


def search(dni):
    client = get_client()
    data = client.service.search(dni)
    return process_data(data)


def process_data(data):
    person = {}
    data = helpers.serialize_object(data._value_1)
    for item in data:
        person[item[0].text] = item[1].text

    return person


def wsdl():
    client = get_client()
    return client.wsdl


def get_client():
    return Client(config.get('SERVICE_SOAP_DNI_URL'))
