from dotenv import dotenv_values
from flask import Flask, request
from zeep.exceptions import Fault
from soap import search

import json

config = dotenv_values('../.env')

app = Flask(__name__)


@app.route('/', methods=['GET'])
def home():
    return ''


@app.route('/<dni>', methods=['GET'])
def get_person_get(dni):
    error = True
    message = ''

    try:
        if dni:
            data = search(dni)
            response = {'error': False, 'data': data}
        else:
            response = {'error': True, 'message': 'No se encontraron resultados'}
    except Fault as error:
        response = {'error': True, 'message': error.message}
    except:
        response = {'error': True, 'message': 'Se produjo un error inesperado'}

    return app.response_class(
        response=json.dumps(response),
        status=200,
        mimetype='application/json'
    )


@app.route('/', methods=['POST'])
def get_person():
    error = True
    message = ''

    response = {'error': True, 'message': 'No se encontraron resultados'}

    try:
        if request.get_json():
            dni = request.json.get('dni')
            if dni:
                data = search(dni)
                response = {'error': False, 'data': data}
    except Fault as error:
        response = {'error': True, 'message': error.message}
    except:
        response = {'error': True, 'message': 'Se produjo un error inesperado'}

    return app.response_class(
        response=json.dumps(response),
        status=200,
        mimetype='application/json'
    )


if __name__ == '__main__':
    app.run(debug=config.get('DEBUG'), host=config.get('FLASK_APP_HOST'), port=config.get('FLASK_APP_PORT'))
